# Cloud & IaaS Basics Project | Java App
We will create a remote server using DigitalOcean droplets and deploy a Java application to it. I've included the built JAR file but the source will be downloaded and built later as well.

# Technologies Used
- DigitalOcean Droplets
- Linux (Ubuntu)
- Java
- Gradle

# Project Description
- Setup and configure a server on DigitalOcean
- Create and configure a new Linux user on the Droplet (security best practice!)
- Deploy and run a Java Gradle application on the Droplet

# Prerequisites
- Local machine (I will be using Ubuntu, any OS is fine but steps may vary depending on tools/commands used)
	- Your machine should have Git, Java, and Gradle installed. If not, you can use `apt install` for all three.
		- Java version on local machine is java-17-openjdk-amd64
- DigitalOcean account and ability to create a droplet
	- Configure it with your personal SSH key from your local machine

# Steps for Setup

## Configure DigitalOcean

### Create Droplet
1. Create an account if you don't already have one
2. Create a new project
3. Create a new Droplet
	- Location = closest to you, we'll use NY
	- Ubuntu 22.10 x64
	- Basic Plan
	- Regular (SSD) / $4/month
	- Authentication > SSH
		- Add your **public** SSH key from your local machine here
			- cat ~/.ssh/id_rsa.pub on Linux
			- If you don't have a key you can use the `ssh-keygen` command on Linux
4.  Finish by pressing **Create Droplet**

![Droplet Created](images/m5-1-droplet-made.png)

### Configure Firewall
1. DigitalOcean Menu > Droplets > Networking > Firewalls > Edit > Create Firewall
	- Set a name for the firewall that is descriptive
	- Set inbound SSH (port 22) ability for your IP address
		- You can get your IP by going to https://www.whatismyip.com/ or alternative methods
	- Set inbound port 7071 as open to all ipv4 and ipv6 addresses (This is for the application we will be deploying)
	- **Create Firewall**
2. Go to Networking > Firewall > Droplets > Add Droplet
	- Add your new Droplet to this firewall for it to take effect

![Configured Firewall](images/m5-2-firewall-settings.png)

### Confirm Droplet Setup Complete
1. From your local machine, you will now be able to remote in by using `ssh root@droplet_ip`
	- *Note* using root is not best practice, we will create a new user later

![Successful SSH to Droplet](images/m5-3-ssh-success.png)

## Configure Droplet
1. Install Java
	- `sudo apt update`
	- `sudo apt install openjdk-8-jre-headless`
	- `java -version` to confirm install
2. Add new service user
	- `adduser java-svc`
	- `usermod -aG sudo java-svc`
	- `su jira-svc`
3. Add SSH ability for jira-svc
	- `mkdir ~/.ssh`
	- `sudo vim ~/.ssh/authorized_keys`
	- Paste the output of `cat ~/.ssh/id_rsa.pub` from your local machine into this file and save it (`:wq`)

## Download and Build Project Files
### Local Machine
1. `mkdir ~/repository/m5`
2. `cd ~/repository/m5`
3. `git clone https://gitlab.com/twn-devops-bootcamp/latest/05-cloud/java-react-example.git`
4. `cd java-react-example`
5. `gradle build`

![JAR file built successfully](images/m5-4-jar-built-successfully.png)

## Upload and Run Project
### Local Machine
1. `scp build/libs/java-react-example.jar jira-svc@SERVER_IP:~`
	- Use your SERVER_IP that you copied from your droplets DigitalOcean portal

![Upload success](images/m5-5-upload-success.png)

### Remote Droplet
1. You should already be SSH'd onto the server with your jira-svc account or as root. If you are on root, you can use `su jira-svc` to switch user
2. `cd ~`
3. `java -jar java-react-example.jar &` 

![Program Running](images/m5-6-app-running.png)
## Test the Project
1. From your web browser go to DROPLET_IP:7071 and you should get your website loaded in.
	- There is not certificate installed so you may need to allow your browser to access the "unsafe" site.

![Web App is running and accessible](images/m5-7-application-available.png)
